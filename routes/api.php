<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HotelDataController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'hoteldata'], function () {

    Route::get('/all', [HotelDataController::class, 'getAll']);
    Route::get('/brands', [HotelDataController::class, 'getBrands']);
    Route::get('/chains', [HotelDataController::class, 'getChains']);
    Route::get('/cities', [HotelDataController::class, 'getCities']);
    Route::get('/states', [HotelDataController::class, 'getStates']);
    Route::get('/search', [HotelDataController::class, 'getSearch']);
    
});
