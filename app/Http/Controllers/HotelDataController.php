<?php

namespace App\Http\Controllers;

use App\Models\HotelData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HotelDataController extends Controller
{
 public function getAll()
 {
  $data = HotelData::simplePaginate(10);
  return \response([
   'success' => true,
   'data'    => $data,
  ]);
 }
 /**
  * Get all brands
  */
 public function getBrands()
 {
  $data = Cache::rememberForever('hotel-brands', function () {
   HotelData::select(DB::raw('brand_name as brand, count(*) as total'))
    ->where('brand_name', '<>', '')
    ->where('brand_name', '<>', 'No brand')
    ->groupBy('brand_name')
    ->orderBy('total', 'desc')
    ->get();

  });

  return \response([
   'success' => true,
   'data'    => $data,
  ]);

 }
 /**
  * Get all chains anme
  */
 public function getChains()
 {

  $data = Cache::rememberForever('hotel-chains', function () {
   return HotelData::select(DB::raw('chain_name as chain, count(*) as total'))
    ->groupBy('chain_name')
    ->where('chain_name', '<>', 'No Chain')
    ->orderBy('total', 'desc')
    ->get();

  });

  return \response([
   'success' => true,
   'data'    => $data,
  ]);
 }
 /**
  * Get all cities anme
  */
 public function getCities()
 {

  $data = Cache::rememberForever('hotel-cities', function () {
   return HotelData::select(DB::raw('city as city, count(*) as total,city_id'))
    ->groupBy('city')
    ->where('city', '<>', '')
    ->orderBy('total', 'desc')
    ->get();

  });

  return \response([
   'success' => true,
   'data'    => $data,
  ]);
 }

 /**
  * Get all states anme
  */
 public function getStates()
 {

  $data = Cache::rememberForever('hotel-states', function () {
   return HotelData::select(DB::raw('state as state, count(*) as total'))
    ->groupBy('state')
    ->where('state', '<>', '')
   //  ->where('state', '!=', null)
    ->orderBy('total', 'desc')
    ->get();

  });

  return \response([
   'success' => true,
   'data'    => $data,
  ]);
 }

 public function getSearch(Request $request)
 {

  if ($request->keyword) {

   $data = Cache::remember('search-hotel-' . Str::slug($request->keyword), 300, function () use ($request) {

    return HotelData::where('hotel_name', 'like', '%' . $request->keyword . '%')
     ->orWhere('hotel_name', 'like', '%' . $request->keyword . '%')
     ->orWhere('hotel_formerly_name', 'like', '%' . $request->keyword . '%')
     ->orWhere('hotel_translated_name', 'like', '%' . $request->keyword . '%')
     ->orWhere('city', 'like', '%' . $request->keyword . '%')
     ->orWhere('state', 'like', '%' . $request->keyword . '%')
     ->simplePaginate(10)->appends(['keyword' => $request->keyword]);

   });
  } elseif ($request->city) {

   $data = Cache::remember('search-hotel-by-city', 300, function () use ($request) {
    return HotelData::Where('city', 'like', '%' . $request->city . '%')
     ->orWhere('state', 'like', '%' . $request->city . '%')
     ->simplePaginate(10)->appends(['city' => $request->city]);
   });

  }

  return \response([
   'success' => true,
   'data'    => $data,
  ]);

 }

}
